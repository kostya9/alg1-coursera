import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by kostya on 7/1/2017.
 */
public class BruteCollinearPoints {
    private final LineSegment[] segments;

    private class Tuple {
        public Point start;
        public Point end;

        @Override
        public boolean equals(Object obj) {
            if(!(obj instanceof Tuple))
                return false;

            Tuple that = (Tuple) obj;

            return this.start.compareTo(that.start) == 0
                    && this.end.compareTo(that.end) == 0;
        }
    }
    private boolean theSameSegmentExist(List<Tuple> lines, Tuple line) {
        return lines.contains(line);
    }

    public BruteCollinearPoints(Point[] points) {
        if(points == null)
            throw new IllegalArgumentException();

        for (int i = 0; i < points.length; i++) {
            Point p = points[i];
            if (p == null)
                throw new IllegalArgumentException();
            if(isDuplicate(points, i))
                throw new IllegalArgumentException();
        }

        List<LineSegment> segmentList = new ArrayList<>();
        List<Tuple> lines = new ArrayList<>();
        for(int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length && j != i; j++) {
                for (int k = j + 1; k < points.length && k != j; k++) {
                    for (int m = k + 1; m < points.length && m != k && m != i; m++) {
                        if (areOnTheSameSlope(points, i, j, k, m)
                                && areDistinct(points, i, j, k, m)) {
                            Point start = min(points, i, j, k, m);
                            Point end = max(points, i, j, k, m);
                            Tuple lineTuple = new Tuple();
                            lineTuple.start = start;
                            lineTuple.end = end;
                            LineSegment line = new LineSegment(start, end);
                            if(!theSameSegmentExist(lines, lineTuple)) {
                                segmentList.add(line);
                                lines.add(lineTuple);
                            }

                        }
                    }
                }
            }
        }

        LineSegment[] segmentsArray = new LineSegment[segmentList.size()];
        this.segments = segmentList.toArray(segmentsArray);
    }

    private boolean isDuplicate(Point[] points, int i) {
        for (int j = 0; j < i; j++) {
            if(points[j].compareTo(points[i]) == 0)
                return true;
        }

        return false;
    }

    private Point max(Point[] points, int... indices) {
        Point max = points[indices[0]];
        for(int i = 1; i < indices.length; i++)
        {
            if(points[indices[i]].compareTo(max) > 0)
                max = points[indices[i]];
        }

        return max;
    }

    private Point min(Point[] points, int... indices) {
        Point min = points[indices[0]];
        for(int i = 1; i < indices.length; i++)
        {
            if(points[indices[i]].compareTo(min) < 0)
                min = points[indices[i]];
        }

        return min;
    }

    private boolean areDistinct(Point[] points, int i, int j, int k, int m) {
        return points[i].compareTo(points[j]) != 0
                && points[i].compareTo(points[k]) != 0
                && points[i].compareTo(points[m]) != 0
                && points[j].compareTo(points[k]) != 0
                && points[k].compareTo(points[m]) != 0
                && points[j].compareTo(points[m]) != 0;
    }

    private boolean areOnTheSameSlope(Point[] points, int i, int j, int k, int m) {
        Comparator<Point> order = points[i].slopeOrder();
        return order.compare(points[j], points[k]) == 0 &&
                order.compare(points[k], points[m]) == 0;
    }

    public int numberOfSegments() {
        return segments.length;
    }

    public LineSegment[] segments() {
        return segments.clone();
    }

    public static void main(String[] args) {
        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
