import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.*;

/**
 * Created by kostya on 7/1/2017.
 */
public class FastCollinearPoints {
    private final LineSegment[] segments;

    private class Tuple {
        public Point start;
        public Point end;

        @Override
        public boolean equals(Object obj) {
            if(!(obj instanceof Tuple))
                return false;

            Tuple that = (Tuple) obj;

            return this.start.compareTo(that.start) == 0
                    && this.end.compareTo(that.end) == 0;
        }
    }
    private boolean theSameSegmentExist(List<Tuple> lines, Tuple line) {
        return lines.contains(line);
    }

    public FastCollinearPoints(Point[] points) {
        if(points == null)
            throw new IllegalArgumentException();
        List<LineSegment> segmentsList = new ArrayList<>();
        List<Tuple> lines = new ArrayList<>();

        for (int i = 0; i < points.length; i++) {
            Point p = points[i];
            if (p == null)
                throw new IllegalArgumentException();
            if(isDuplicate(points, i))
                throw new IllegalArgumentException();
        }

        for (int i = 0; i < points.length; i++) {
            Point point = points[i];
            Comparator<Point> order = point.slopeOrder();
            Point[] sorted = points.clone();
            Arrays.sort(sorted, order);
            for (int j = 0; j < sorted.length; j++) {
                int from = j;
                while (++j < sorted.length && order.compare(sorted[from], sorted[j]) == 0);
                int to = --j;
                if (to - from >= 2) { // 3 or more adjacent points
                    Point start = min(sorted, from, to, point);
                    Point end = max(sorted, from, to, point);
                    Tuple lineTuple = new Tuple();
                    lineTuple.start = start;
                    lineTuple.end = end;
                    LineSegment line = new LineSegment(start, end);
                    if(!theSameSegmentExist(lines, lineTuple)) {
                        lines.add(lineTuple);
                        segmentsList.add(line);
                    }
                }
            }
        }

        LineSegment[] segmentsArray = new LineSegment[segmentsList.size()];
        this.segments = segmentsList.toArray(segmentsArray);
    }

    private boolean isDuplicate(Point[] points, int i) {
        for (int j = 0; j < i; j++) {
            if(points[j].compareTo(points[i]) == 0)
                return true;
        }

        return false;
    }

    private Point max(Point[] points, int from, int to, Point current) {
        Point max = current;
        for(int i = from; i <= to; i++)
        {
            if(points[i].compareTo(max) > 0)
                max = points[i];
        }

        return max;
    }

    private Point min(Point[] points, int from, int to, Point current) {
        Point min = current;
        for(int i = from; i <= to; i++)
        {
            if(points[i].compareTo(min) < 0)
                min = points[i];
        }

        return min;
    }

    public int numberOfSegments() {
        return segments.length;
    }
    public LineSegment[] segments() {
        return segments.clone();
    }

    public static void main(String[] args) {
        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
