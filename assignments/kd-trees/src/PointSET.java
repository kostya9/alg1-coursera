import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;

import java.util.*;

/**
 * Created by kostya on 7/30/2017.
 */
public class PointSET {
    private final TreeSet<Point2D> points;

    public PointSET() {
        points = new TreeSet<>();
    }

    public boolean isEmpty() {
        return points.isEmpty();
    }

    public int size() {
        return points.size();
    }

    public void insert(Point2D p) {
        if(p == null)
            throw new IllegalArgumentException();

        points.add(p);
    }

    public boolean contains(Point2D p) {
        if(p == null)
            throw new IllegalArgumentException();

        return points.contains(p);
    }

    public void draw() {
        for (Point2D point: points) {
            point.draw();
        }
    }

    public Iterable<Point2D> range(RectHV rect) {
        if(rect == null)
            throw new IllegalArgumentException();

        List<Point2D> pointsInRange = new ArrayList<>();
        if (isEmpty())
            return pointsInRange;
        Set<Point2D> sub = points.subSet(new Point2D(rect.xmin(), rect.ymin()), true, new Point2D(rect.xmax(), rect.ymax()), true);
        for (Point2D p: sub) {
            if(rect.contains(p))
                pointsInRange.add(p);
        }
        return pointsInRange;
    }

    public Point2D nearest(Point2D p) {
        if(p == null)
            throw new IllegalArgumentException();

        if(isEmpty())
            return null;

        Point2D curNearest = points.first();
        for (Point2D curP: points) {
            if(curNearest.distanceTo(p) > curP.distanceTo(p))
                curNearest = curP;
        }

        return curNearest;
    }
}
