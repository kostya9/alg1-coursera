import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kostya on 7/30/2017.
 */
public class KdTree {
    private Node root;
    private int size = 0;

    private class Node {
        Node left;
        Node right;
        Point2D value;
        Node(Point2D value) {
            this.value = value;
        }
    }

    public KdTree(){

    }

    public boolean isEmpty() {
        return root == null;
    }

    public int size() {
        return size;
    }

    public void insert(Point2D p) {
        if(p == null)
            throw new IllegalArgumentException();
        if(contains(p))
            return;
        root = insert(root, p, 0);
        size++;
    }

    private Node insert(Node node, Point2D p, int height) {
        if(node == null)
            return new Node(p);

        int cmp = compare(p, node.value, height);

        if(cmp < 0) node.left = insert(node.left, p, height + 1);
        else node.right = insert(node.right, p, height + 1);

        return node;
    }

    private int compare(Point2D first, Point2D second, int height) {
         return isVertical(height) ? Double.compare(first.x(), second.x())
                 : Double.compare(first.y(),  second.y());
    }

    private boolean isVertical(int height) {
        return height % 2 == 0;
    }

    public boolean contains(Point2D p) {
        if(p == null)
            throw new IllegalArgumentException();

        if(isEmpty())
            return false;

        Node node = root;
        int height = 0;

        while (node != null) {
            if(node.value.equals(p))
                return true;
            int cmp = compare(p, node.value, height++);
            if(cmp < 0)
                node = node.left;
            else
                node = node.right;
        }

        return false;
    }

    private boolean intersects(Node n, RectHV rect, int height) {
        if(isVertical(height))
            return  n.value.x() >= rect.xmin() && n.value.x() <= rect.xmax();
        else
            return  n.value.y() >= rect.ymin() && n.value.y() <= rect.ymax();
    }

    private void range(Node node, RectHV rect, int height, List<Point2D> points) {
        if(node == null)
            return;

        if(intersects(node, rect, height)) {
            if(rect.contains(node.value))
                points.add(node.value);

            range(node.left, rect, height + 1, points);
            range(node.right, rect, height + 1, points);
            return;
        }

        int cmp = isVertical(height) ? Double.compare(rect.xmax(), node.value.x())
                : Double.compare(rect.ymax(), node.value.y());

        if(cmp < 0)
            range(node.left, rect, height + 1, points);
        else
            range(node.right, rect, height + 1, points);

    }

    public Iterable<Point2D> range(RectHV rect) {
        List<Point2D> points = new ArrayList<>();
        range(root, rect, 0, points);
        return points;
    }

    private Point2D nearest(Node n, Point2D p, int height) {
        if(n == null)
            return null;
        if(n.left == null && n.right == null)
            return n.value;

        double distance = p.distanceSquaredTo(n.value);

        int cmp = compare(p, n.value, height);
        Node first;
        Node other;
        if(cmp < 0) {
            first = n.left;
            other = n.right;
        }
        else {
            first = n.right;
            other = n.left;
        }

        RectHV curRect = isVertical(height) ? new RectHV(n.value.x(), 0, n.value.x(), 1)
                : new RectHV(0, n.value.y(), 1, n.value.y());

        Point2D firstPoint = nearest(first, p, height + 1);
        if(firstPoint == null) {
            firstPoint = nearest(other, p, height + 1);
            return firstPoint.distanceSquaredTo(p) > distance ? n.value : firstPoint;
        }

        Point2D min = firstPoint.distanceSquaredTo(p) > distance ? n.value : firstPoint;

        if (other == null || firstPoint.distanceSquaredTo(p) < curRect.distanceSquaredTo(p))
            return min;

        Point2D otherPoint = nearest(other, p, height + 1);

        return  min.distanceSquaredTo(p) < otherPoint.distanceSquaredTo(p) ? min : otherPoint;

    }

    public Point2D nearest(Point2D p) {
        if(isEmpty())
            return null;
        return nearest(root, p, 0);
    }

    private void draw(Node n, double top, double right , double bot, double left, int height) {
        if(n == null)
            return;

        StdDraw.setPenColor(StdDraw.BLACK);
        n.value.draw();

        if(isVertical(height)) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.line(n.value.x(), top, n.value.x(), bot);
            draw(n.left, top, n.value.x(), bot, left, height + 1);
            draw(n.right, top, right, bot, n.value.x(), height + 1);
        } else {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.line(left, n.value.y(), right, n.value.y());
            draw(n.left, n.value.y(), right, bot, left, height + 1);
            draw(n.right, top, right, n.value.y(), left, height + 1);
        }
    }

    public void draw() {
        draw(root, 1, 1, 0, 0, 0);
    }

    public static void main(String[] args) {
        String filename = args[0];
        In in = new In(filename);

        StdDraw.enableDoubleBuffering();

        // initialize the data structures with N points from standard input
        PointSET brute = new PointSET();
        KdTree kdtree = new KdTree();
        while (!in.isEmpty()) {
            double x = in.readDouble();
            double y = in.readDouble();
            Point2D p = new Point2D(x, y);
            kdtree.insert(p);
            brute.insert(p);
        }

        double x0 = 0.0, y0 = 0.0;      // initial endpoint of rectangle
        double x1 = 0.0, y1 = 0.0;      // current location of mouse
        boolean isDragging = false;     // is the user dragging a rectangle

        // draw the points
        StdDraw.clear();
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        brute.draw();
        StdDraw.show();

        while (true) {

            // user starts to drag a rectangle
            if (StdDraw.mousePressed() && !isDragging) {
                x0 = StdDraw.mouseX();
                y0 = StdDraw.mouseY();
                isDragging = true;
                continue;
            }

            // user is dragging a rectangle
            else if (StdDraw.mousePressed() && isDragging) {
                x1 = StdDraw.mouseX();
                y1 = StdDraw.mouseY();
                continue;
            }

            // mouse no longer pressed
            else if (!StdDraw.mousePressed() && isDragging) {
                isDragging = false;
            }


            RectHV rect = new RectHV(Math.min(x0, x1), Math.min(y0, y1),
                    Math.max(x0, x1), Math.max(y0, y1));
            // draw the points
            StdDraw.clear();
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(0.01);
            brute.draw();

            // draw the rectangle
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius();
            rect.draw();

            // draw the range search results for brute-force data structure in red
            StdDraw.setPenRadius(0.03);
            StdDraw.setPenColor(StdDraw.RED);
            for (Point2D p : brute.range(rect))
                p.draw();

            // draw the range search results for kd-tree in blue
            StdDraw.setPenRadius(.02);
            StdDraw.setPenColor(StdDraw.BLUE);
            for (Point2D p : kdtree.range(rect))
                p.draw();

            StdDraw.show();
            StdDraw.pause(40);
        }
    }


}
