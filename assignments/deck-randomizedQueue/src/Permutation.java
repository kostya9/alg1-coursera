import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by kostya on 6/18/2017.
 */
public class Permutation {
    public static void main(String[] args) throws FileNotFoundException {
        if(args.length > 1)
            System.setIn(new FileInputStream(args[1]));

        int numbersToDisplayCount = Integer.parseInt(args[0]);
        RandomizedQueue<String> queue = new RandomizedQueue<>();
        while (!StdIn.isEmpty())
            queue.enqueue(StdIn.readString());
        for (int i = 0; i < numbersToDisplayCount; i++) {
            if(queue.isEmpty())
                break;
            StdOut.println(queue.dequeue());
        }
    }
}
