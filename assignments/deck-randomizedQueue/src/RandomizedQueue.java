import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by kostya on 6/15/2017.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] items;
    private int size;

    public RandomizedQueue() {
        items = (Item[]) new Object[2];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator(items, size);
    }

    public int size() {
        return size;
    }

    public Item dequeue() {
        if(isEmpty())
            throw new NoSuchElementException();

        int index = StdRandom.uniform(size);

        Item dequeueItem = items[index];
        size--;
        if(size != 0 && index != size) {
            items[index] = items[size];
        }
        items[size] = null;

        if(size != 0 && size == items.length / 4) {
            resize(items.length / 2);
        }

        return dequeueItem;
    }

    public void enqueue(Item item) {
        if(item == null)
            throw new NullPointerException();

        if(size == items.length)
            resize(items.length * 2);

        items[size] = item;
        size++;
    }

    public Item sample() {
        if(isEmpty())
            throw new NoSuchElementException();

        int index = StdRandom.uniform(size);
        return items[index];
    }

    private void resize(int newSize) {
        Item[] newItems = (Item[]) new Object[newSize];
        for(int i = 0; i < size; i++) {
            newItems[i] = items[i];
        }

        items = newItems;
    }

    private class RandomizedQueueIterator implements Iterator<Item> {

        private Item[] items;
        private int current;
        private int size;

        public RandomizedQueueIterator(Item[] items, int size) {
            this.items = (Item[]) new Object[size];
            this.size = size;
            for (int i = 0; i < size; i++) {
                this.items[i] = items[i];
            }

            StdRandom.shuffle(this.items);
        }

        @Override
        public boolean hasNext() {
            return current != size;
        }

        @Override
        public Item next() {
            if(!hasNext())
                throw new NoSuchElementException();

            Item current = items[this.current];
            this.current++;
            return current;
        }
    }
}
