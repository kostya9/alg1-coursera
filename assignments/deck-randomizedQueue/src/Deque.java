import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by kostya on 6/15/2017.
 */
public class Deque<Item> implements Iterable<Item> {

    private class Node {
        Item item;
        Node next;
        Node prev;
    }

    private Node head;
    private Node tail;
    private int size = 0;

    public Deque() {

    }

    public boolean isEmpty() {
        return head == null;
    }

    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        validateAdd(item);

        Node newHead = new Node();
        newHead.item = item;
        newHead.next = head;
        if(head!= null)
            head.prev = newHead;
        head = newHead;
        if(tail == null)
            tail = head;
        size++;
    }

    private void validateAdd(Item item) {
        if(item == null)
            throw new NullPointerException();
    }

    public void addLast(Item item) {
        validateAdd(item);

        Node newTail = new Node();
        newTail.item = item;
        newTail.prev = tail;
        if(tail != null)
            tail.next = newTail;
        tail = newTail;

        if(head == null)
            head = tail;
        size++;
    }

    public Item removeFirst() {
        validateRemove();

        Node prevHead = head;
        head = head.next;
        if(head != null)
            head.prev = null;
        else
            tail = null;

        size--;
        return prevHead.item;
    }

    public Item removeLast() {
        validateRemove();

        Node prevTail = tail;
        tail = prevTail.prev;
        if(tail != null)
            tail.next = null;
        else
            head = null;

        size--;
        return prevTail.item;
    }

    private void validateRemove() {
        if(isEmpty())
            throw new NoSuchElementException();
    }

    public Iterator<Item> iterator() {
        return new DequeIterator(head);
    }

    private class DequeIterator implements Iterator<Item> {

        private Node next;

        public DequeIterator(Node head) {
            if(head == null)
            {
                next = null;
                return;
            }

            next = copy(head);
            Node prev = next;
            while (prev.next != null) {
                prev.next = copy(prev.next);
                prev.next.prev = prev;
                prev = prev.next;
            }
        }

        private Node copy(Node from) {
            Node out = new Node();
            out.next = from.next;
            out.prev = from.prev;
            out.item = from.item;
            return out;
        }

        @Override
        public boolean hasNext() {
            return next != null;
        }

        @Override
        public Item next() {
            if(!hasNext())
                throw new NoSuchElementException();
            Node cur = next;
            next = next.next;
            return cur.item;
        }
    }
}
