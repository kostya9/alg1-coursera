import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;

/**
 * Created by kostya on 6/15/2017.
 */
public class DequeTest {

    @Test
    public void testAdd() {
        Deque<String> deque = new Deque<>();
        deque.addFirst("123");
        deque.addFirst("1234");
        deque.addLast("12345");
        deque.addLast("123456");

        assertEquals(4, deque.size());
    }

    @Test
    public void testRemove() {
        Deque<String> deque = new Deque<>();

        deque.addLast("1234");
        deque.addLast("12345");
        deque.addLast("123456");
        deque.addLast("1234567");

        assertEquals("1234", deque.removeFirst());
        assertEquals("1234567", deque.removeLast());
        assertEquals(2, deque.size());
    }

    @Test
    public void testOneElement() {
        Deque<String> deque = new Deque<>();

        deque.addLast("1234");

        assertEquals("1234", deque.removeFirst());

        deque.addFirst("12345");

        assertEquals("12345", deque.removeLast());
    }

    @Test
    public void testIterator() {
        Deque<String> deque = new Deque<>();

        deque.addLast("1234");
        deque.addLast("12345");
        deque.addLast("123456");
        deque.addLast("1234567");
        deque.addFirst("123");

        int i = 3;
        String expected = "123";
        for (String el : deque) {
            assertEquals(expected, el);
            expected += ++i;
        }
        assertEquals(i - 3, deque.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void testEmptyIterator() {
        Deque<String> deque = new Deque<>();

        Iterator iterator = deque.iterator();
        assertEquals(false, iterator.hasNext());
        iterator.next();
    }


}
