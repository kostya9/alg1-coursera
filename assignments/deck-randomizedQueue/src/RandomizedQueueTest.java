
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by kostya on 6/16/2017.
 */
public class RandomizedQueueTest {
    private static boolean contains(Iterable<String> iterable, String item) {
        for (String el : iterable) {
            if(Objects.equals(el, item))
                return true;
        }
        return false;
    }

    @Test
    public void enqueueTest() {
        RandomizedQueue<String> queue = new RandomizedQueue<>();
        queue.enqueue("1234");
        queue.enqueue("12345");
        queue.enqueue("123456");

        assertEquals(true, contains(queue, "1234"));
        assertEquals(true, contains(queue, "12345"));
        assertEquals(true, contains(queue, "123456"));
    }

    @Test
    public void dequeueTest() {
        RandomizedQueue<String> queue = new RandomizedQueue<>();
        String[] values = new String[6];

        values[0] = "1234";
        values[1] = "12345";
        values[2] = "123456";
        values[3] = "1234567";
        values[4] = "12345678";
        values[5] = "123456789";

        for (String val  : values) {
            queue.enqueue(val);
        }

        List<String> valuesList = Arrays.asList(values);

        String s1 = queue.dequeue();
        assertEquals(true, contains(valuesList, s1));
        String s2 = queue.dequeue();
        assertEquals(true, contains(valuesList, s2));

        assertEquals(false, contains(queue, s1));
        assertEquals(false, contains(queue, s2));

        for (String val : values) {
            if(Objects.equals(val, s1) || val.equals(s2))
                continue;
            assertEquals(true, contains(queue, val));
        }

    }

    @Test
    public void testIsEmpty() {
        RandomizedQueue<String> queue = new RandomizedQueue<>();

        assertEquals(true, queue.isEmpty());

        queue.enqueue("1234");
        queue.enqueue("12345");
        queue.enqueue("123456");
        queue.dequeue();
        assertEquals(false, queue.isEmpty());
        queue.dequeue();
        queue.dequeue();
        assertEquals(true, queue.isEmpty());
    }

    @Test
    public void sampleTest() {
        RandomizedQueue<String> queue = new RandomizedQueue<>();
        queue.enqueue("1234");
        queue.enqueue("12345");
        queue.enqueue("123456");

        String sample = queue.sample();
        assertEquals(true, contains(queue, sample));
        sample = queue.sample();
        assertEquals(true, contains(queue, sample));
    }
}
