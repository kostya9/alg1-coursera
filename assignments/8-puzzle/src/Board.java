import java.util.Iterator;
import java.util.Stack;

/**
 * Created by kostya on 7/9/2017.
 */
public final class Board {
    private final int[][] blocks;
    private final int manhattan;

    public Board(int[][] blocks) {
        this.blocks = clone(blocks);
        this.manhattan = computeManhattan();
    }

    public int dimension() {
        return blocks.length;
    }

    public int hamming() {
        int score = 0;
        for (int i = 0; i < blocks.length; i++) {
            for (int j = 0; j < blocks.length; j++) {
                if(blocks[i][j] != 0 && blocks[i][j] != i * blocks.length + j + 1)
                    score++;
            }
        }

        return score;
    }

    private int computeManhattan() {
        int score = 0;
        for (int i = 0; i < blocks.length; i++) {
            for (int j = 0; j < blocks.length; j++) {
                if(blocks[i][j] == 0)
                    continue;
                int rowGoal = (blocks[i][j] - 1) / blocks.length;
                int columnGoal = (blocks[i][j] - 1) % blocks.length;
                score += Math.abs(rowGoal - i) + Math.abs(columnGoal - j);

            }
        }

        return score;
    }

    public int manhattan() {
        return manhattan;
    }

    public boolean isGoal() {
        return manhattan == 0;
    }

    public Board twin() {
        int[][] twinBlocks = clone(blocks);

        int prevI = 0, prevJ = 0;
        boolean toBreak = false;
        for (int i = 0; i < 2 && !toBreak; i++) {
            for (int j = 0; j < 2 && !toBreak; j++) {
                if((i + j != 0) && blocks[i][j] != 0 && blocks[prevI][prevJ] != 0) {
                    swap(twinBlocks, i, j, prevI, prevJ);
                    toBreak = true;
                }
                prevJ = j;
            }
            prevI = i;
        }
        return new Board(twinBlocks);
    }

    private static void swap(int arr[][], int fromRow, int fromColumn, int toRow, int toColumn) {
        int tmp = arr[fromRow][fromColumn];
        arr[fromRow][fromColumn] = arr[toRow][toColumn];
        arr[toRow][toColumn] = tmp;
    }

    public boolean equals(Object y) {
        if(!(y instanceof Board))
            return false;

        Board that = (Board)y;
        if(this.dimension() != that.dimension())
            return false;

        for (int i = 0; i < blocks.length; i++) {
            for (int j = 0; j < blocks.length; j++) {
                if(this.blocks[i][j] != that.blocks[i][j])
                    return false;
            }
        }

        return true;
    }

    public Iterable<Board> neighbors() {
        return new BoardNeighborsIterable(blocks);
    }

    private static int[][] clone(int[][] arr) {
        int[][] clone = new int[arr.length][];
        for (int i = 0; i < arr.length; i++) {
            clone[i] = arr[i].clone();
        }

        return clone;
    }

    private class BoardNeighborsIterable implements Iterable<Board> {
        private Stack<Board> neighbors;
        public BoardNeighborsIterable(int[][] board) {
            neighbors = new Stack<>();
            int row = 0, column = 0;
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board.length; j++) {
                    if(board[i][j] == 0) {
                        row = i;
                        column = j;
                    }
                }
            }

            int[][] newBoard = Board.this.clone(board);
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 2; j++) {
                    int deltaRow = 0, deltaColumn = 0;
                    if(i == 0) {
                        deltaRow = j == 0 ? -1 : 1;
                    }
                    else {
                        deltaColumn = j == 0 ? -1 : 1;
                    }
                    int destRow = row + deltaRow;
                    int destColumn = column + deltaColumn;
                    if(destRow >= 0 && destColumn >= 0 && destRow < board.length && destColumn < board.length) {
                        swap(newBoard, row, column, destRow, destColumn);
                        neighbors.add(new Board(newBoard));
                        swap(newBoard, row, column, destRow, destColumn); // undo the swap
                    }
                }
            }

        }

        @Override
        public Iterator<Board> iterator() {
            return neighbors.iterator();
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(dimension() + "\n");
        for (int[] block : blocks) {
            for (int j = 0; j < blocks.length; j++) {
                builder.append(String.format(" %d ", block[j]));
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        int[][] arr = new int[3][];
        arr[0] = new int[]{3, 2, 1};
        arr[1] = new int[]{4, 5, 6};
        arr[2] = new int[]{0, 8, 7};
        Board board = new Board(arr);
        assert board.hamming() == 3;
        assert board.manhattan() == 6;

        swap(arr, 0, 0, 1, 0);
        board = new Board(arr);
        assert board.hamming() == 4;
        assert board.manhattan() == 8;

        System.out.println(board.toString());
        for (Board b : board.neighbors()) {
            System.out.println(b.toString());
        }

    }


}
