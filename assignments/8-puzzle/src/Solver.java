import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Created by kostya on 7/9/2017.
 */
public class Solver {
    private SearchNode result;
    private int moves;
    public Solver(Board initial) {
        if(initial == null)
            throw new IllegalArgumentException();

        Comparator<SearchNode> nodesComparator = new Comparator<SearchNode>() {
            private int getManhattanPriority(SearchNode node) {
                return node.board.manhattan() + node.moves;
            }
            @Override
            public int compare(SearchNode o1, SearchNode o2) {
                return Integer.compare(getManhattanPriority(o1),  getManhattanPriority(o2));
            }
        };

        MinPQ<SearchNode> queueMain = new MinPQ<SearchNode>(nodesComparator);
        MinPQ<SearchNode> queueTwin = new MinPQ<SearchNode>(nodesComparator);
        queueMain.insert(new SearchNode(initial, 0, null));
        queueTwin.insert(new SearchNode(initial.twin(), 0, null));
        while (!queueMain.min().board.isGoal() && !queueTwin.min().board.isGoal())
        {
            SearchNode minMain = queueMain.delMin();
            SearchNode minTwin = queueTwin.delMin();
            for(Board board : minMain.board.neighbors()) {
                if(minMain.prevNode != null && board.equals(minMain.prevNode.board))
                    continue;
                queueMain.insert(new SearchNode(board, minMain.moves + 1, minMain));
            }

            for(Board board : minTwin.board.neighbors()) {
                if(minTwin.prevNode != null && board.equals(minTwin.prevNode.board))
                    continue;
                queueTwin.insert(new SearchNode(board, minTwin.moves + 1, minTwin));
            }
        }

        if(queueTwin.min().board.isGoal()) {
            moves = -1;
            result = null;
            return;
        }

        result = queueMain.min();
        moves = result.moves;
    }

    public boolean isSolvable() {
        return moves() != -1;
    }

    public int moves() {
        return moves;
    }

    public Iterable<Board> solution() {
        if(!isSolvable())
            return null;
        Stack<Board> solution = new Stack<>();
        SearchNode cur = result;
        while (cur != null) {
            solution.push(cur.board);
            cur = cur.prevNode;
        }

        return new Iterable<Board>() {
            @Override
            public Iterator<Board> iterator() {
                return solution.iterator();
            }
        };
    }

    private class SearchNode {
        final Board board;
        final SearchNode prevNode;
        final int moves;

        public SearchNode(Board board, int moves, SearchNode prevBoard) {
            this.board = board;
            this.prevNode = prevBoard;
            this.moves = moves;
        }
    }

    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}
