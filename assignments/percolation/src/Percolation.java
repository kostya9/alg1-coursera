import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**
 * Created by kostya on 6/10/2017.
 */
public class Percolation {
    private WeightedQuickUnionUF pathChecker;
    private boolean[][] sites;

    public Percolation(int gridSize) {
        if(gridSize <= 0)
            throw new IllegalArgumentException();

        pathChecker = new WeightedQuickUnionUF(gridSize * gridSize + 2);

        sites = new boolean[gridSize][];
        for (int i = 0; i < gridSize; i++) {
            sites[i] = new boolean[gridSize];
        }
    }

    public void open(int row, int column) {
        validate(row, column);

        if(isOpen(row, column))
            return;

        if(row == 1)
            pathChecker.union(getTopVirtualSiteIndex(), toIndex(row, column));

        if(row == getGridSize())
            pathChecker.union(getBottomVirtualSiteIndex(), toIndex(row, column));

        if(column != 1)
            unionIfOpen(row, column, row, column - 1);

        if(row != getGridSize())
            unionIfOpen(row, column, row + 1, column);

        if(column != getGridSize())
            unionIfOpen(row, column, row, column + 1);

        if(row != 1)
            unionIfOpen(row, column, row - 1, column);

        sites[row - 1][column - 1] = true;
    }

    public boolean isOpen(int row, int column) {
        validate(row, column);
        return sites[row - 1][column - 1];
    }

    public boolean isFull(int row, int column) {
        validate(row, column);
        return isOpen(row, column) && pathChecker.connected(getTopVirtualSiteIndex(), toIndex(row, column));
    }

    public int numberOfOpenSites() {
        int sum = 0;
        for (boolean[] siteRows : sites) {
            for (boolean site: siteRows) {
                if(site)
                    sum++;
            }
        }

        return sum;
    }

    public boolean percolates() {
        if(getGridSize() == 1)
        {
            return sites[0][0];
        }
        return pathChecker.connected(getTopVirtualSiteIndex(), getBottomVirtualSiteIndex());
    }

    private int getBottomVirtualSiteIndex() {
        return getGridSize() * getGridSize() + 1;
    }

    private int getTopVirtualSiteIndex() {
        return getGridSize() * getGridSize();
    }

    private void unionIfOpen(int centerRow, int centerColumn, int sideRow, int sideColumn) {
        if(isOpen(sideRow, sideColumn))
            pathChecker.union(toIndex(centerRow, centerColumn), toIndex(sideRow, sideColumn));
    }

    private int getGridSize() {
        return sites.length;
    }

    private int toIndex(int row, int column) {
        return (row  - 1) * getGridSize() + column - 1;
    }

    private void validate(int row, int column) {
        if(row < 1 || column < 1 || row > getGridSize() || column > getGridSize())
            throw new IndexOutOfBoundsException();
    }
}
