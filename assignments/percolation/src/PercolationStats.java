import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

/**
 * Created by kostya on 6/10/2017.
 */
public class PercolationStats {
    private double mean;
    private double stdDev;
    private int trials;

    public PercolationStats(int gridSize, int trials) {
        if(gridSize <= 0 || trials <= 0)
            throw new IllegalArgumentException();

        this.trials = trials;
        double[] openSitesFractions = new double[trials];

        for (int i = 0; i < trials; i++) {
            Percolation percolation = new Percolation(gridSize);

            while (!percolation.percolates())
                percolation.open(StdRandom.uniform(gridSize) + 1, StdRandom.uniform(gridSize) + 1);

            openSitesFractions[i] = ((double)percolation.numberOfOpenSites()) / (gridSize * gridSize);
        }

        mean = StdStats.mean(openSitesFractions);
        stdDev = StdStats.stddev(openSitesFractions);
    }

    public double mean() {
        return mean;
    }

    public double stddev() {
        return stdDev;
    }

    public double confidenceLo() {
        return mean - 1.96 * stdDev / Math.sqrt(trials);
    }

    public double confidenceHi() {
        return mean + 1.96 * stdDev / Math.sqrt(trials);
    }

    public static void main(String[] args) {
        if(args.length != 2)
        {
            System.out.println("Insufficient command line arguments. Expected two integers: gridSize and trials");
            return;
        }

        int gridSize = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);

        PercolationStats stats = new PercolationStats(gridSize, trials);
        StdOut.println(leftSide("mean")+ stats.mean());
        StdOut.println(leftSide("stddev") + stats.stddev());
        StdOut.println(leftSide("95% confidence interval") + "[ "+ stats.confidenceLo() + ", " + stats.confidenceHi() + "]");
    }

    private static String leftSide(String text) {
        return (padded(text) + " = ");
    }

    private static String padded(String s) {
        return String.format("%0$-23s", s);
    }
}
