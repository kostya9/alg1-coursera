package UnionFind;

/**
 * Created by kostya9 on 10.06.17.
 */
public class WeightedQuickUnion extends QuickUnion {
    private int[] treeLengths;

    public WeightedQuickUnion(int maxNumber) {
        super(maxNumber);

        treeLengths = new int[maxNumber];
        for (int i = 0; i < maxNumber; i++) {
            treeLengths[i] = 1;
        }
    }

    @Override
    public void union(int first, int second) {

        if(first == second)
            return;

        int firstRoot = getRoot(first);
        int secondRoot = getRoot(second);

        if(firstRoot == secondRoot)
            return;

        if(treeLengths[firstRoot] < treeLengths[secondRoot])
            changeRoots(firstRoot, secondRoot);
        else
            changeRoots(secondRoot, firstRoot);
    }

    private void changeRoots(int from, int to) {
        ids[from] = to;
        treeLengths[to] += treeLengths[from];
    }
}
