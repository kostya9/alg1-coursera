package UnionFind;

/**
 * Created by kostya9 on 09.06.17.
 */
public class QuickUnion implements PathCheck{
    protected int[] ids;

    public QuickUnion(int maxNumber) {
        ids = new int[maxNumber];

        for (int i = 0; i < maxNumber; i++) {
            ids[i] = i;
        }
    }

    @Override
    public boolean connected(int first, int second) {
        return getRoot(first) == getRoot(second);
    }

    @Override
    public void union(int first, int second) {
        if(first < 0 || second < 0 || first >= ids.length || second >= ids.length)
            return;

        ids[getRoot(first)] = getRoot(second);
    }

    protected int getRoot(int number) {
        if(ids[number] == number)
            return number;

        return getRoot(ids[number]);
    }
}
