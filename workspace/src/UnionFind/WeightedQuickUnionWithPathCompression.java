package UnionFind;

/**
 * Created by kostya9 on 10.06.17.
 */
public class WeightedQuickUnionWithPathCompression extends WeightedQuickUnion {
    public WeightedQuickUnionWithPathCompression(int maxNumber) {
        super(maxNumber);
    }

    @Override
    protected int getRoot(int number) {
        int root = super.getRoot(number);
        ids[number] = ids[root];
        return root;
    }
}
