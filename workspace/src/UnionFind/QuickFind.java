package UnionFind;

/**
 * Created by kostya9 on 09.06.17.
 */
public class QuickFind implements PathCheck {
    int[] ids;

    public QuickFind(int maxNumber) {
        ids = new int[maxNumber];

        for(int i = 0; i < maxNumber; i++) {
            ids[i] = i;
        }
    }

    public void union(int first, int second) {
        int firstId = ids[first];
        int secondId = ids[second];


        for(int i = 0; i < ids.length; i++) {
            if(ids[i] == firstId)
                ids[i] = secondId;
        }
    }

    public boolean connected(int first, int second) {
        return ids[first] == ids[second];
    }
}
