package UnionFind.MonteCarlo;

import UnionFind.PathCheck;
import UnionFind.WeightedQuickUnionWithPathCompression;
import com.sun.javafx.geom.Vec2d;

import java.util.Random;

/**
 * Created by kostya9 on 10.06.17.
 */
public class BruteForceMonteCarloSimulator {
    private Random random = new Random();
    protected PathCheck pathCheck;
    protected IndexToVectorTranslator translator;
    private boolean[][] sites;

    public BruteForceMonteCarloSimulator(int gridSize) {
        pathCheck = new WeightedQuickUnionWithPathCompression(gridSize * gridSize);
        sites = new boolean[gridSize][];
        for (int i = 0; i < gridSize; i++) {
            sites[i] = new boolean[gridSize];
        }

        translator = new IndexToVectorTranslator(getGridSize());
    }

    private int getGridSize() {
        return sites.length;
    }

    public double getSiteVacancyProbability() {
        int vacantCount = 0;
        for (int i = 0; i < getGridSize(); i++) {
            for (int j = 0; j < getGridSize(); j++) {
                if(sites[i][j])
                    vacantCount++;
            }
        }

        return ((double) vacantCount)/(getGridSize() * getGridSize());
    }

    public boolean isPercolated() {
        for (int i = 0; i < getGridSize(); i++) {
            for (int j = 0; j < getGridSize(); j++) {
                Vec2d top = new Vec2d(i, 0);
                Vec2d bottom = new Vec2d(j, getGridSize() - 1);
                if(pathCheck.connected(translator.toIndex(top), translator.toIndex(bottom)))
                    return true;
            }
        }

        return false;
    }

    public void setRandomOpen() {
        int index = random.nextInt(getGridSize() * getGridSize());
        Vec2d coordinates = translator.toCoordinates(index);
        setOpen(coordinates);
    }

    private void setOpen(Vec2d siteCoordinate) {
        if(sites[(int) siteCoordinate.x][(int) siteCoordinate.y])
            return;

        int site = translator.toIndex(siteCoordinate);

        Vec2d left = new Vec2d(siteCoordinate.x - 1, siteCoordinate.y);
        Vec2d top = new Vec2d(siteCoordinate.x, siteCoordinate.y + 1);
        Vec2d right = new Vec2d(siteCoordinate.x + 1, siteCoordinate.y);
        Vec2d bottom = new Vec2d(siteCoordinate.x, siteCoordinate.y - 1);


        connectIfOpen(site, left);
        connectIfOpen(site, top);
        connectIfOpen(site, right);
        connectIfOpen(site, bottom);

        sites[(int) siteCoordinate.x][(int) siteCoordinate.y] = true;
    }

    private void connectIfOpen(int siteIndex, Vec2d to) {
        if(to.y < 0 || to.x < 0 || to.x >= getGridSize() || to.y >= getGridSize())
            return;
        if(sites[(int)to.x][(int)to.y])
            pathCheck.union(siteIndex, translator.toIndex(to));
    }

}
