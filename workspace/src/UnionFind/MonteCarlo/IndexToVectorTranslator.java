package UnionFind.MonteCarlo;

import com.sun.javafx.geom.Vec2d;

/**
 * Created by kostya9 on 10.06.17.
 */
public class IndexToVectorTranslator {
    private int maxIndex;

    public IndexToVectorTranslator(int maxIndex) {
        this.maxIndex = maxIndex;
    }

    public Vec2d toCoordinates(int index) {
        return new Vec2d(index % maxIndex, index / maxIndex);
    }

    public int toIndex(Vec2d coordinates) {
        return (int) (coordinates.x + coordinates.y * maxIndex);
    }
}
