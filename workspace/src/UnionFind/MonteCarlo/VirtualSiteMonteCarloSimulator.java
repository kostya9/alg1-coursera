package UnionFind.MonteCarlo;

import UnionFind.WeightedQuickUnionWithPathCompression;
import com.sun.javafx.geom.Vec2d;

/**
 * Created by kostya9 on 10.06.17.
 */
public class VirtualSiteMonteCarloSimulator extends BruteForceMonteCarloSimulator {
    private int gridSize;

    public VirtualSiteMonteCarloSimulator(int gridSize) {
        super(gridSize);
        this.gridSize = gridSize;

        pathCheck = new WeightedQuickUnionWithPathCompression(gridSize * gridSize + 2);
        for(int i = 0; i < gridSize; i++) {
            // Top virtual site
            Vec2d top = new Vec2d(i, 0);
            pathCheck.union(gridSize* gridSize, translator.toIndex(top));

            // Bottom virtual site
            Vec2d bottom = new Vec2d(i, gridSize - 1);
            pathCheck.union(gridSize * gridSize + 1, translator.toIndex(bottom));
        }
    }

    @Override
    public boolean isPercolated() {
        return pathCheck.connected(gridSize * gridSize, gridSize * gridSize + 1);
    }
}
