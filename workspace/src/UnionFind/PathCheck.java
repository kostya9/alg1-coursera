package UnionFind;

/**
 * Created by kostya9 on 09.06.17.
 */
public interface PathCheck {
    boolean connected(int first, int second);
    void union(int first, int second);
}
