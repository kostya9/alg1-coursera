import UnionFind.MonteCarlo.BruteForceMonteCarloSimulator;
import UnionFind.MonteCarlo.VirtualSiteMonteCarloSimulator;
import UnionFind.PathCheck;
import UnionFind.WeightedQuickUnionWithPathCompression;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;

/**
 * Created by kostya9 on 09.06.17.
 */
public class Main {
    public static void main(String[] args) {
        int gridSize = 200;
        int times = (int) 1e2;
        //testMonteCarloBrute(gridSize, times);
        testMonteCarloVirtual(gridSize, times);
    }

    public static void testMonteCarloBrute(int gridSize, int times) {
        List<Double> vacancies = new ArrayList<>();
        for (int k = 0; k < times; k++) {
            BruteForceMonteCarloSimulator simulator = new BruteForceMonteCarloSimulator(gridSize);
            while (!simulator.isPercolated())
                simulator.setRandomOpen();

            vacancies.add(simulator.getSiteVacancyProbability());
        }

        //noinspection OptionalGetWithoutIsPresent
        System.out.println(DoubleStream.of(vacancies.stream().mapToDouble(d -> d).toArray()).average().getAsDouble());
    }

    public static void testMonteCarloVirtual(int gridSize, int times) {
        List<Double> vacancies = new ArrayList<>();
        for (int k = 0; k < times; k++) {
            VirtualSiteMonteCarloSimulator simulator = new VirtualSiteMonteCarloSimulator(gridSize);
            while (!simulator.isPercolated())
                simulator.setRandomOpen();

            vacancies.add(simulator.getSiteVacancyProbability());
        }

        //noinspection OptionalGetWithoutIsPresent
        System.out.println(DoubleStream.of(vacancies.stream().mapToDouble(d -> d).toArray()).average().getAsDouble());
    }

    public void testPathCheck() {
        //UnionFind.PathCheck pathCheck = new UnionFind.QuickFind(10);
        //UnionFind.PathCheck pathCheck = new UnionFind.QuickUnion(10);
        //UnionFind.PathCheck pathCheck = new UnionFind.WeightedQuickUnion(10);
        PathCheck pathCheck = new WeightedQuickUnionWithPathCompression(10);
        pathCheck.union(0, 5);
        pathCheck.union(5, 6);
        pathCheck.union(6, 1);
        pathCheck.union(7, 2);
        //pathCheck.union(2, 1);

        pathCheck.union(3, 4);
        pathCheck.union(8, 3);
        pathCheck.union(4, 9);
        pathCheck.union(3, 7);

        pathCheck.connected(7, 2); // to test path compression

        System.out.println(pathCheck.connected(0, 7)); //false
        System.out.println(pathCheck.connected(7, 8)); //true
        System.out.println(pathCheck.connected(8, 9)); //true
    }
}
